#!/usr/bin/env python

import argparse

parser = argparse.ArgumentParser(description="Set Athos energy via undulators")
parser.add_argument("energy", type=float, help="Target energy in eV")
clargs = parser.parse_args()


import os
import sys

slic_path = os.path.dirname(sys.argv[0]) + "/../slic"
sys.path.append(slic_path)


from time import sleep
from devices.undulator import Undulators

und = Undulators(adjust_chic=False)
sleep(1) # give PVs some time to connect


print("before:", und)
print("target:", clargs.energy, "eV")
print("actual change is commented out!")
#und.set(clargs.energy)
print("after: ", und)



